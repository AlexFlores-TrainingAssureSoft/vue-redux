import { createApp } from 'vue'
import { createStore } from 'vuex'
import router from 'vue-router'
import App from './App.vue'

const store = createStore({
    state(){
        return{
            detail:''
        }
    },
    getters:{
        detail:state=>state.detail
    },
    mutations:{
        concat (state,payload){
            state.detail = state.detail + payload
        },
        compute (state){
            state.detail = eval(state.detail);
        },
        clear(state){
            state.detail = '';
        }
    }
})

const app = createApp(App)

app.use(router)
app.use(store)
app.mount('#app')